#!/usr/bin/python

import rospy

from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from std_msgs.msg import String

rospy.init_node('Joystick_to_Velocity_Command')

linear_scale = rospy.get_param('~linear_scale', 1.0)
angular_scale = rospy.get_param('~angular_scale', 1.0)
cmd_vel_topic = rospy.get_param('~cmd_vel_topic', 'cmd_vel')

rospy.loginfo("Linear scaling: %lf", linear_scale)
rospy.loginfo("Angular scaling: %lf", angular_scale)

cmd_vel = Twist()
cmd_vel.linear.x = 0.0
cmd_vel.angular.z = 0.0

def joystick_callback(data):
    global cmd_vel
    # Stop unless X button is held
    block_scaler = 0.0
    boost = 1.0
    if data.buttons[0]:
        block_scaler = 1.0
    if data.buttons[5]:
        boost = 2.0

    cmd_vel.linear.x = data.axes[1] * linear_scale * block_scaler * boost
    cmd_vel.angular.z = data.axes[0] * angular_scale * block_scaler

pub = rospy.Publisher(cmd_vel_topic, Twist, queue_size=1000)

rospy.Subscriber("joy", Joy, joystick_callback)

r = rospy.Rate(10)
while not rospy.is_shutdown():
    pub.publish(cmd_vel)
    r.sleep()
